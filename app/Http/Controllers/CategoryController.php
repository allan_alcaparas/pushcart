<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;


class CategoryController extends Controller
{

	public function index()
	{
		$categories = Category::all();
		return view('categories.index')->with('categories',$categories);
	}

	public function show($category)
	{
		$result = Category::find($category);
		return view('categories.show')->with('category',$result);
	}

    public function create()
    {
    	return view('categories.create');
    }

    public function store(Request $request){
    	$request->validate([
    		'name' => 'string|required|max:5|unique:categories,name'
    	]);
    	$name = $request->input('name');
    	$category = new Category;
    	$category->name = $name;
    	$category->save();
    	return redirect(route('categories.index'));
    }

    public function edit($category)
    {
    	$result = Category::find($category);
    	return view('categories.edit')->with('category',$result);
    }

    public function update(Category $category, Request $request)
    {
    	//$result = Category::find($category);
    	$request->validate([
    		'name' => 'string|required|max:5|unique:categories,name'
    	]);
    	$category->name = $request->input('name');
    	$category->save();
    	return redirect(route('categories.show',['category' => $category->id]));
    }

    public function destroy($category)
    {
    	$result = Category::find($category);
    	$result->delete();
    	return redirect(route('categories.index'));
    }

}
