<ul>
	@foreach($categories as $category)
		<li>
			{{ $category->name }}
			<a href="{{route('categories.show',['category' => $category->id])}}">
				Details
			</a>
			<a href="{{route('categories.edit',['category' => $category->id])}}">
				Update
			</a>
			<form action="{{route('categories.destroy',['category' => $category->id])}}" method="post">
				@csrf
				@method('DELETE')
				<button type="submit">Delete</button>
			</form>
		</li>
	@endforeach
</ul>