@if($errors->any())
	<div>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</div>
@endif
<form action="/categories" method="POST">
	@csrf
	<label for="name">Category Name:</label>
	<input type="text" name="name">
	<button type="submit">Create New Category</button>
</form>