@if($errors->any())
	<div>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</div>
@endif
<form action="{{route('categories.show',['category' => $category->id])}}" method="POST">
	@method('PUT')
	@csrf
	<label for="name">Category Name:</label>
	<input type="text" name="name" value="{{ $category->name }}">
	<button type="submit">Update Category</button>
</form>